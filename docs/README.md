# Go OT (Golang Open Tibia Server)
Otland Forum Thread: https://otland.net/threads/goot-golang-server.264817/  
Discord: https://discord.gg/zVmrKBc

#### How to compile/run files:
https://gitlab.com/wick3dr0se/goots/wiki/Running-Go-OT

#### Contributor guidelines:
https://gitlab.com/wick3dr0se/goots/blob/master/docs/contributing.md
